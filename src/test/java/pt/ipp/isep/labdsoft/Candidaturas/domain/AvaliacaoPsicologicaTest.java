package pt.ipp.isep.labdsoft.Candidaturas.domain;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import pt.ipp.isep.labdsoft.Candidaturas.dto.AvaliacaoMedicaDTO;
import pt.ipp.isep.labdsoft.Candidaturas.dto.AvaliacaoPsicologicaDTO;

@RunWith(SpringJUnit4ClassRunner.class)

public class AvaliacaoPsicologicaTest {

    @Test
    public void fromDTO(){
        AvaliacaoPsicologicaDTO dto = new AvaliacaoPsicologicaDTO("r","p","t");
        AvaliacaoPsicologica av = AvaliacaoPsicologica.fromDTO(dto);

        Assertions.assertEquals(av.getPartilhaDecisao(),dto.partilhaDecisao);
        Assertions.assertEquals(av.getRazaoParticipacao(),dto.razaoParticipacao);
        Assertions.assertEquals(av.getTempoParaDecidir(),dto.tempoParaDecidir);

    }

    @Test
    public void toDTO(){
        AvaliacaoPsicologica av = new AvaliacaoPsicologica("r","p","t");
        AvaliacaoPsicologicaDTO dto = av.toDTO();

        Assertions.assertEquals(av.getPartilhaDecisao(),dto.partilhaDecisao);
        Assertions.assertEquals(av.getRazaoParticipacao(),dto.razaoParticipacao);
        Assertions.assertEquals(av.getTempoParaDecidir(),dto.tempoParaDecidir);

    }

}
