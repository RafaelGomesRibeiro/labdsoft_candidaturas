package pt.ipp.isep.labdsoft.Candidaturas.domain;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import pt.ipp.isep.labdsoft.Candidaturas.dto.ConsultaPreEstudoDTO;
import pt.ipp.isep.labdsoft.Candidaturas.utils.DatesFormatter;

import java.time.LocalDateTime;

@RunWith(SpringJUnit4ClassRunner.class)

public class ConsultaPreEstudoTest {

    @Test
    public void fromDtoTest(){
        String now = DatesFormatter.convertToString(LocalDateTime.now());
        String after = DatesFormatter.convertToString(LocalDateTime.now().plusHours(1));
        ConsultaPreEstudoDTO dto = new ConsultaPreEstudoDTO(now,after,"Gab");
        ConsultaPreEstudo c = ConsultaPreEstudo.fromDTO(dto);
        Assertions.assertEquals(DatesFormatter.convertToLocalDateTime(now),c.getInicio());
        Assertions.assertEquals(DatesFormatter.convertToLocalDateTime(after),c.getFim());
        Assertions.assertEquals(dto.idGabinete,c.getIdGabinete());

    }

    @Test
    public void toDtoTest(){
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime after = LocalDateTime.now().plusHours(1);
        ConsultaPreEstudo c = new ConsultaPreEstudo(now,after,"gab");
        ConsultaPreEstudoDTO dto = c.toDTO();
        Assertions.assertEquals(DatesFormatter.convertToString(c.getInicio()),dto.inicio);
        Assertions.assertEquals(DatesFormatter.convertToString(c.getFim()),dto.fim);
        Assertions.assertEquals(dto.idGabinete,c.getIdGabinete());
    }

}
