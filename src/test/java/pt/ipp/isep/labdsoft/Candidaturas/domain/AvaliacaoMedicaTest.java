package pt.ipp.isep.labdsoft.Candidaturas.domain;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import pt.ipp.isep.labdsoft.Candidaturas.dto.AvaliacaoMedicaDTO;

@RunWith(SpringJUnit4ClassRunner.class)

public class AvaliacaoMedicaTest {
    @Test
    public void fromDto(){
        AvaliacaoMedicaDTO dto = new AvaliacaoMedicaDTO(true,false,false,true,"a",true,"3",Protocolo.PROTOCOLO_A.toString(),TipoSangue.A.toString(),TipoGene.AA.toString());
        AvaliacaoMedica av = AvaliacaoMedica.fromDTO(dto);
        Assertions.assertEquals(av.isDiabetes(),dto.diabetes);
        Assertions.assertEquals(av.isRins(),dto.rins);
        Assertions.assertEquals(av.isOperacao(),dto.operacao);
        Assertions.assertEquals(av.isCoracao(),dto.coracao);
        Assertions.assertEquals(av.getRazaoOperacao(),dto.razaoOperacao);
        Assertions.assertEquals(av.isFumador(),dto.fumador);
        Assertions.assertEquals(av.getNumCigarros(),dto.numCigarros);
        Assertions.assertEquals(av.getProtocolo().toString(),dto.protocolo);
        Assertions.assertEquals(av.getTipoSangue().toString(),dto.tipoSangue);
        Assertions.assertEquals(av.getTipoGene().toString(),dto.tipoGene);
    }
    @Test
    public void toDto(){
        AvaliacaoMedica av = new AvaliacaoMedica(true,false,false,true,"a",true,"3",Protocolo.PROTOCOLO_A,TipoSangue.A,TipoGene.AA);
        AvaliacaoMedicaDTO dto = av.toDTO();
        Assertions.assertEquals(av.isDiabetes(),dto.diabetes);
        Assertions.assertEquals(av.isRins(),dto.rins);
        Assertions.assertEquals(av.isOperacao(),dto.operacao);
        Assertions.assertEquals(av.isCoracao(),dto.coracao);
        Assertions.assertEquals(av.getRazaoOperacao(),dto.razaoOperacao);
        Assertions.assertEquals(av.isFumador(),dto.fumador);
        Assertions.assertEquals(av.getNumCigarros(),dto.numCigarros);
        Assertions.assertEquals(av.getProtocolo().toString(),dto.protocolo);
        Assertions.assertEquals(av.getTipoSangue().toString(),dto.tipoSangue);
        Assertions.assertEquals(av.getTipoGene().toString(),dto.tipoGene);
    }
}
