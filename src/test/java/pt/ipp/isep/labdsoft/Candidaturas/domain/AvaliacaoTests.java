package pt.ipp.isep.labdsoft.Candidaturas.domain;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import pt.ipp.isep.labdsoft.Candidaturas.dto.AvaliacaoDTO;
import pt.ipp.isep.labdsoft.Candidaturas.dto.AvaliacaoMedicaDTO;
import pt.ipp.isep.labdsoft.Candidaturas.dto.AvaliacaoPsicologicaDTO;
import pt.ipp.isep.labdsoft.Candidaturas.dto.ConsultaPreEstudoDTO;
import pt.ipp.isep.labdsoft.Candidaturas.utils.DatesFormatter;

import java.time.LocalDateTime;

@RunWith(SpringJUnit4ClassRunner.class)

public class AvaliacaoTests {


    @Test
    public void fromDTOTest(){
        AvaliacaoMedicaDTO mDto = new AvaliacaoMedicaDTO(true,false,false,true,"a",true,"3",Protocolo.PROTOCOLO_A.toString(),TipoSangue.A.toString(),TipoGene.AA.toString());
        AvaliacaoPsicologicaDTO pDto = new AvaliacaoPsicologicaDTO("r","p","t");
        String now = DatesFormatter.convertToString(LocalDateTime.now());
        String after = DatesFormatter.convertToString(LocalDateTime.now().plusHours(1));
        ConsultaPreEstudoDTO cDto = new ConsultaPreEstudoDTO(now,after,"Gab");
        AvaliacaoDTO avDto = new AvaliacaoDTO(1L,2L,3L,4L,true,false,mDto,pDto,cDto);
        Avaliacao av = Avaliacao.fromDTO(avDto);
        //AV
        Assertions.assertEquals(avDto.idMedico, av.getIdMedico());
        Assertions.assertEquals(avDto.idPsicologo, av.getIdPsicologo());
        Assertions.assertEquals(avDto.idUtente, av.getIdUtente());
        Assertions.assertEquals(avDto.parecerMedico, av.getParecerMedico());
        Assertions.assertEquals(avDto.parecerPsicologico, av.getParecerPsicologico());
        //MED
        Assertions.assertEquals(av.getAvaliacaoMedica().isDiabetes(),mDto.diabetes);
        Assertions.assertEquals(av.getAvaliacaoMedica().isRins(),mDto.rins);
        Assertions.assertEquals(av.getAvaliacaoMedica().isOperacao(),mDto.operacao);
        Assertions.assertEquals(av.getAvaliacaoMedica().isCoracao(),mDto.coracao);
        Assertions.assertEquals(av.getAvaliacaoMedica().getRazaoOperacao(),mDto.razaoOperacao);
        Assertions.assertEquals(av.getAvaliacaoMedica().isFumador(),mDto.fumador);
        Assertions.assertEquals(av.getAvaliacaoMedica().getNumCigarros(),mDto.numCigarros);
        Assertions.assertEquals(av.getAvaliacaoMedica().getProtocolo().toString(),mDto.protocolo);
        Assertions.assertEquals(av.getAvaliacaoMedica().getTipoSangue().toString(),mDto.tipoSangue);
        Assertions.assertEquals(av.getAvaliacaoMedica().getTipoGene().toString(),mDto.tipoGene);
        //PSIC
        Assertions.assertEquals(av.getAvaliacaoPsicologica().getPartilhaDecisao(),pDto.partilhaDecisao);
        Assertions.assertEquals(av.getAvaliacaoPsicologica().getRazaoParticipacao(),pDto.razaoParticipacao);
        Assertions.assertEquals(av.getAvaliacaoPsicologica().getTempoParaDecidir(),pDto.tempoParaDecidir);
        //CONS
        Assertions.assertEquals(DatesFormatter.convertToLocalDateTime(now),av.getConsulta().getInicio());
        Assertions.assertEquals(DatesFormatter.convertToLocalDateTime(after),av.getConsulta().getFim());
        Assertions.assertEquals(cDto.idGabinete,av.getConsulta().getIdGabinete());
    }


    @Test
    public void toDtoTest(){
        AvaliacaoMedica avM = new AvaliacaoMedica(true,false,false,true,"a",true,"3",Protocolo.PROTOCOLO_A,TipoSangue.A,TipoGene.AA);
        AvaliacaoPsicologica avP = new AvaliacaoPsicologica("r","p","t");

        LocalDateTime now = LocalDateTime.now();
        LocalDateTime after = LocalDateTime.now().plusHours(1);
        ConsultaPreEstudo c = new ConsultaPreEstudo(now,after,"gab");

        Avaliacao av = new Avaliacao(1L,2L,3L,4L,true,false,avM,avP,c);
        AvaliacaoDTO avDto = av.toDTO();
        //AV
        Assertions.assertEquals(avDto.idMedico, av.getIdMedico());
        Assertions.assertEquals(avDto.idPsicologo, av.getIdPsicologo());
        Assertions.assertEquals(avDto.idUtente, av.getIdUtente());
        Assertions.assertEquals(avDto.parecerMedico, av.getParecerMedico());
        Assertions.assertEquals(avDto.parecerPsicologico, av.getParecerPsicologico());
        //MED
        Assertions.assertEquals(av.getAvaliacaoMedica().isDiabetes(),avDto.avaliacaoMedica.diabetes);
        Assertions.assertEquals(av.getAvaliacaoMedica().isRins(),avDto.avaliacaoMedica.rins);
        Assertions.assertEquals(av.getAvaliacaoMedica().isOperacao(),avDto.avaliacaoMedica.operacao);
        Assertions.assertEquals(av.getAvaliacaoMedica().isCoracao(),avDto.avaliacaoMedica.coracao);
        Assertions.assertEquals(av.getAvaliacaoMedica().getRazaoOperacao(),avDto.avaliacaoMedica.razaoOperacao);
        Assertions.assertEquals(av.getAvaliacaoMedica().isFumador(),avDto.avaliacaoMedica.fumador);
        Assertions.assertEquals(av.getAvaliacaoMedica().getNumCigarros(),avDto.avaliacaoMedica.numCigarros);
        Assertions.assertEquals(av.getAvaliacaoMedica().getProtocolo().toString(),avDto.avaliacaoMedica.protocolo);
        Assertions.assertEquals(av.getAvaliacaoMedica().getTipoSangue().toString(),avDto.avaliacaoMedica.tipoSangue);
        Assertions.assertEquals(av.getAvaliacaoMedica().getTipoGene().toString(),avDto.avaliacaoMedica.tipoGene);
        //PSIC
        Assertions.assertEquals(av.getAvaliacaoPsicologica().getPartilhaDecisao(),avDto.avaliacaoPsicologica.partilhaDecisao);
        Assertions.assertEquals(av.getAvaliacaoPsicologica().getRazaoParticipacao(),avDto.avaliacaoPsicologica.razaoParticipacao);
        Assertions.assertEquals(av.getAvaliacaoPsicologica().getTempoParaDecidir(),avDto.avaliacaoPsicologica.tempoParaDecidir);
        //CONS
        Assertions.assertEquals(DatesFormatter.convertToString(now),avDto.consultaPreEstudo.inicio);
        Assertions.assertEquals(DatesFormatter.convertToString(after),avDto.consultaPreEstudo.fim);
        Assertions.assertEquals(avDto.consultaPreEstudo.idGabinete,av.getConsulta().getIdGabinete());
    }

}
