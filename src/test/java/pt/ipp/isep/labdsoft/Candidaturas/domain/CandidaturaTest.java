package pt.ipp.isep.labdsoft.Candidaturas.domain;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import pt.ipp.isep.labdsoft.Candidaturas.dto.CandidaturaDTO;
import pt.ipp.isep.labdsoft.Candidaturas.dto.DoencaDTO;

import java.sql.Date;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
public class CandidaturaTest {

    @Test
    public void fromDto(){
        LocalDateTime now = LocalDateTime.now();
        Date d = Date.valueOf(now.toLocalDate());
        DoencaDTO dtoDoenca= new DoencaDTO("dor");
        List<DoencaDTO> dtosDoencas = new ArrayList<>();
        dtosDoencas.add(dtoDoenca);
        CandidaturaDTO dto = new CandidaturaDTO("ze","zao","rua",d,"pais","nax", "2","mail","prof","grau","estado",true,dtosDoencas);
        Candidatura c = Candidatura.fromDto(dto);

        Assertions.assertEquals(c.nome(),dto.nome);
        Assertions.assertEquals(c.sobrenome(),dto.sobrenome);
        Assertions.assertEquals(c.morada(),dto.morada);
        Assertions.assertEquals(c.dataNascimento(),dto.dataNascimento);
        Assertions.assertEquals(c.localNascimento(),dto.localNascimento);
        Assertions.assertEquals(c.cartaoCidadao(),dto.cartaoCidadao);
        Assertions.assertEquals(c.nacionalidade(),dto.nacionalidade);
        Assertions.assertEquals(c.profissao(),dto.profissao);
        Assertions.assertEquals(c.estadoCivil(),dto.estadoCivil);
        Assertions.assertEquals(c.grauEscolaridade(),dto.grauEscolaridade);
        Assertions.assertEquals(c.veiculoProprio(),dto.veiculoProprio);
        Assertions.assertEquals(c.doencas().size(),dto.doencas.size());

        for (int i = 0; i < dtosDoencas.size(); i++) {
            Assertions.assertEquals(dto.doencas.get(i).descricao,c.doencas().get(i).getDescricao());
        }



    }

    @Test
    public void toDto(){
        LocalDateTime now = LocalDateTime.now();
        Date d = Date.valueOf(now.toLocalDate());
        List<Doenca> ds = new ArrayList<>();
        Doenca doenca = new Doenca("dd");
        Doenca doenca1 = new Doenca("d1d");
        Doenca doenca2 = new Doenca("dd2");
        ds.add(doenca2);
        ds.add(doenca1);

        ds.add(doenca);
        Candidatura c = new Candidatura("ze","zao","rua",d,"pais","nax", "2","prof","grau","estado",true,ds);
        CandidaturaDTO dto = c.toDTO();

        Assertions.assertEquals(c.nome(),dto.nome);
        Assertions.assertEquals(c.sobrenome(),dto.sobrenome);
        Assertions.assertEquals(c.morada(),dto.morada);
        Assertions.assertEquals(c.dataNascimento(),dto.dataNascimento);
        Assertions.assertEquals(c.localNascimento(),dto.localNascimento);
        Assertions.assertEquals(c.cartaoCidadao(),dto.cartaoCidadao);
        Assertions.assertEquals(c.nacionalidade(),dto.nacionalidade);
        Assertions.assertEquals(c.profissao(),dto.profissao);
        Assertions.assertEquals(c.estadoCivil(),dto.estadoCivil);
        Assertions.assertEquals(c.grauEscolaridade(),dto.grauEscolaridade);
        Assertions.assertEquals(c.veiculoProprio(),dto.veiculoProprio);
        Assertions.assertEquals(c.doencas().size(),dto.doencas.size());

        for (int i = 0; i < ds.size(); i++) {
            Assertions.assertEquals(dto.doencas.get(i).descricao,c.doencas().get(i).getDescricao());
        }


    }

}
