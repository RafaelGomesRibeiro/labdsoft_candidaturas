package pt.ipp.isep.labdsoft.Candidaturas.domain;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


@RunWith(SpringJUnit4ClassRunner.class)

public class DoencaTest {

    @Test
    public void ensureDoencaIsCreatedWithCorrectAttributes(){
        String expectedDescription = "M";
        Doenca d = Doenca.builder().descricao(expectedDescription).build();
        String actualDescription = d.getDescricao();
        Assertions.assertEquals(expectedDescription, actualDescription, "A descrição da doença é incorreta");
    }
}
