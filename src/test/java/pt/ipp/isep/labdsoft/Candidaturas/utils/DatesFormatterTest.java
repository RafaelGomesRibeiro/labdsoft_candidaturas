package pt.ipp.isep.labdsoft.Candidaturas.utils;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.LocalDateTime;

@RunWith(SpringJUnit4ClassRunner.class)
public class DatesFormatterTest {

    @Test
    public void ensureDateFromStringIsCorrectlyParsed(){
        String myDate = "10-10-2010 14:10";
        LocalDateTime expectedDate = LocalDateTime.of(2010,10,10,14,10);
        LocalDateTime actualDate = DatesFormatter.convertToLocalDateTime(myDate);
        Assertions.assertEquals(expectedDate, actualDate, "Dates are not the same");
    }
}
