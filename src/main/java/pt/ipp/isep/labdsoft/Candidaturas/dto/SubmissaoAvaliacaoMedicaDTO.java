package pt.ipp.isep.labdsoft.Candidaturas.dto;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString

public class SubmissaoAvaliacaoMedicaDTO {
    public boolean diabetes;
    public boolean coracao;
    public boolean rins;
    public boolean operacao;
    public String razaoOperacao;
    public boolean fumador;
    public String numCigarros;
    public boolean parecer;
    public String protocolo;
    public String tipoSangue;
    public String tipoGene;

    public AvaliacaoMedicaDTO toAvaliacaoMedicaDTO(){
        return new AvaliacaoMedicaDTO(this.diabetes, this.coracao, this.rins, this.operacao, this.razaoOperacao, this.fumador, this.numCigarros, this.protocolo, this.tipoSangue, this.tipoGene);
    }
}
