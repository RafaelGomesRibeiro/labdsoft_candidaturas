package pt.ipp.isep.labdsoft.Candidaturas.services.rabbitMQ.listener;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public final class EquipaMedicaAlocadaEvent {
    private String dataInicio;
    private String dataFim;
    private String utenteId;
    private String medicoId;
    private String psicologoId;
}
