package pt.ipp.isep.labdsoft.Candidaturas.domain;

import lombok.*;
import pt.ipp.isep.labdsoft.Candidaturas.dto.AvaliacaoMedicaDTO;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
public final class AvaliacaoMedica implements Serializable {
    //@OneToOne
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private boolean diabetes;
    private boolean coracao;
    private boolean rins;
    private boolean operacao;
    private String razaoOperacao;
    private boolean fumador;
    private String numCigarros;
    private Protocolo protocolo;
    private TipoSangue tipoSangue;
    private TipoGene tipoGene;

    public AvaliacaoMedica(boolean diabetes, boolean coracao, boolean rins, boolean operacao, String razaoOperacao, boolean fumador, String numCigarros, Protocolo protocolo, TipoSangue tipoSangue, TipoGene tipoGene) {
        this.diabetes = diabetes;
        this.coracao = coracao;
        this.rins = rins;
        this.operacao = operacao;
        this.razaoOperacao = razaoOperacao;
        this.fumador = fumador;
        this.numCigarros = numCigarros;
        this.protocolo = protocolo;
        this.tipoSangue = tipoSangue;
        this.tipoGene = tipoGene;
    }

    public static AvaliacaoMedica fromDTO(AvaliacaoMedicaDTO dto) {
        return new AvaliacaoMedica(dto.diabetes, dto.coracao, dto.rins, dto.operacao, dto.razaoOperacao, dto.fumador, dto.numCigarros, Protocolo.valueOf(dto.protocolo), TipoSangue.valueOf(dto.tipoSangue), TipoGene.valueOf(dto.tipoGene));
    }

    public AvaliacaoMedicaDTO toDTO() {
        return new AvaliacaoMedicaDTO(this.diabetes, this.coracao, this.rins, this.operacao, this.razaoOperacao, this.fumador, this.numCigarros,
                this.protocolo != null ? this.protocolo.toString() : null, this.tipoSangue != null ? this.tipoSangue.toString() : null, this.tipoGene != null ? this.tipoGene.toString() : null);
    }
}