package pt.ipp.isep.labdsoft.Candidaturas.services;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.encrypt.Encryptors;
import org.springframework.security.crypto.encrypt.TextEncryptor;
import org.springframework.security.crypto.keygen.KeyGenerators;


public final class AES256Encrypter {

    private AES256Encrypter() {
    }

    public static AES256Encrypter newInstance() {
        return new AES256Encrypter();
    }

    @Value("${ENCRYPTER_PASSWORD}")
    private String password;

    public String generateSalt() {
        return KeyGenerators.string().generateKey();
    }

    public String encryptString(final String plainStr, final String salt) {
        TextEncryptor encryptor = Encryptors.text(password, salt);
        return encryptor.encrypt(plainStr);
    }

    public String decryptString(final String encryptedString, final String salt) {
        TextEncryptor encryptor = Encryptors.text(password, salt);
        return encryptor.decrypt(encryptedString);
    }
}
