package pt.ipp.isep.labdsoft.Candidaturas.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pt.ipp.isep.labdsoft.Candidaturas.domain.Avaliacao;

import java.util.Collection;
import java.util.Optional;

@Repository
public interface AvaliacaoRepository extends JpaRepository <Avaliacao, Long> {

     Optional<Avaliacao> findByIdUtente(Long idUtente);
     Collection<Avaliacao> findAvaliacaosByIdMedico(Long idMedico);
     Collection<Avaliacao> findAvaliacaosByIdPsicologo(Long idPsicologo);

}
