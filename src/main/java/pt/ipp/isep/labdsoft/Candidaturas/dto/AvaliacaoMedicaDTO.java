package pt.ipp.isep.labdsoft.Candidaturas.dto;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString

public class AvaliacaoMedicaDTO {
    public boolean diabetes;
    public boolean coracao;
    public boolean rins;
    public boolean operacao;
    public String razaoOperacao;
    public boolean fumador;
    public String numCigarros;
    public String protocolo;
    public String tipoSangue;
    public String tipoGene;
}
