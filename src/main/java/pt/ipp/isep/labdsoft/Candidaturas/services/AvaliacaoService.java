package pt.ipp.isep.labdsoft.Candidaturas.services;

import pt.ipp.isep.labdsoft.Candidaturas.dto.AvaliacaoDTO;
import pt.ipp.isep.labdsoft.Candidaturas.dto.SubmissaoAvaliacaoMedicaDTO;
import pt.ipp.isep.labdsoft.Candidaturas.dto.SubmissaoAvaliacaoPsicologicaDTO;

import java.util.List;
import java.util.Map;

public interface AvaliacaoService {
    List<AvaliacaoDTO> listAll();
    AvaliacaoDTO createAvaliacao(Long idUtente, Long idMedico, Long idPsicologo, String inicio, String fim);
    AvaliacaoDTO byId(Long id);
    AvaliacaoDTO byUtente(Long idUtente);
    AvaliacaoDTO updateAvaliacaoPsicologica(Long id, SubmissaoAvaliacaoPsicologicaDTO avaliacaoPsicologica);
    AvaliacaoDTO updateAvaliacaoMedica(Long id, SubmissaoAvaliacaoMedicaDTO avaliacaoMedica);
    AvaliacaoDTO setGabinete(Long idUtente, String idGabinete);
    Iterable<AvaliacaoDTO> byMedicoPorRealizar(Long idMedico);
    Iterable<AvaliacaoDTO> byPsicologoPorRealizar(Long idPsicologo);
    Long medicoIdByUtente(Long idUtente);
    Map<String, List<Long>> getUtentesByProtocolo();
    Map<String, List<Long>> getUtentesByTipoGene();
}
