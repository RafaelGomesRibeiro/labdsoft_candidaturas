package pt.ipp.isep.labdsoft.Candidaturas.services;

import pt.ipp.isep.labdsoft.Candidaturas.dto.CandidaturaDTO;
import pt.ipp.isep.labdsoft.Candidaturas.dto.CredenciaisDTO;

import java.util.List;


public interface CandidaturaService {

     List<CandidaturaDTO> listAll();
     CredenciaisDTO createCandidatura(CandidaturaDTO c);
     CandidaturaDTO byId(Long id);
}
