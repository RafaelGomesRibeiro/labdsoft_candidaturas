package pt.ipp.isep.labdsoft.Candidaturas;

import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import pt.ipp.isep.labdsoft.Candidaturas.services.AES256Encrypter;


@Configuration
public class CandidaturasConfiguration implements WebMvcConfigurer {

    @Bean
    public MessageConverter jsonMessageConverter() {
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    public AES256Encrypter encrypter() { return AES256Encrypter.newInstance();}
}
