package pt.ipp.isep.labdsoft.Candidaturas.services.rabbitMQ.listener;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public final class GabineteAlocadoEvent {
    private String utenteId;
    private String gabineteId;
}
