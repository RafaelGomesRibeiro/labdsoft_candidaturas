package pt.ipp.isep.labdsoft.Candidaturas.services.rabbitMQ.dispatcher;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;

@Component
public final class EventDispatcher {

    private static final String CANDIDATURAS_EXCHANGE_KEY = "candidaturas";
    private static final String CANDIDATURA_SUBMETIDA_KEY = "A";
    private static final String PARECER_MEDICO_SUBMETIDO_KEY = "B";

    private RabbitTemplate rabbitTemplate;

    public EventDispatcher(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    public void dispatchCandidaturaSubmetidaEvent(CandidaturaSubmetidaEvent event) {
        rabbitTemplate.convertAndSend(CANDIDATURAS_EXCHANGE_KEY, CANDIDATURA_SUBMETIDA_KEY, event);
    }

    public void dispatchParecerMedicoSubmetidoEvent(ParecerMedicoSubmetidoEvent event) {
        rabbitTemplate.convertAndSend(CANDIDATURAS_EXCHANGE_KEY, PARECER_MEDICO_SUBMETIDO_KEY, event);
    }

}
