package pt.ipp.isep.labdsoft.Candidaturas.domain;

import lombok.*;
import lombok.experimental.Accessors;
import pt.ipp.isep.labdsoft.Candidaturas.dto.CandidaturaDTO;
import pt.ipp.isep.labdsoft.Candidaturas.dto.DoencaDTO;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Accessors(fluent = true)
@Builder
public final class Candidatura implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String nome;
    private String sobrenome;
    private String morada;
    @Temporal(TemporalType.DATE)
    private Date dataNascimento;
    private String localNascimento;
    @Column(unique = true)
    private String cartaoCidadao;
    private String nacionalidade;
    private String profissao;
    private String grauEscolaridade;
    private String estadoCivil;
    private boolean veiculoProprio;
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "candidatura_id")
    private List<Doenca> doencas;
    private String salt; // encryption purposes, theres no need to encrypt the salt https://crypto.stackexchange.com/questions/3681/why-not-encrypt-salt
    //falta incluir foto do candidato


    public Candidatura(String nome, String sobrenome, String morada, Date dataNascimento, String localNascimento, String cartaoCidadao,
                       String nacionalidade, String profissao, String grauEscolaridade, String estadoCivil, boolean veiculoProprio, List<Doenca> doencas) {
        this.nome = nome;
        this.sobrenome = sobrenome;
        this.morada = morada;
        this.dataNascimento = dataNascimento;
        this.localNascimento = localNascimento;
        this.cartaoCidadao = cartaoCidadao;
        this.nacionalidade = nacionalidade;
        this.profissao = profissao;
        this.grauEscolaridade = grauEscolaridade;
        this.estadoCivil = estadoCivil;
        this.veiculoProprio = veiculoProprio;
        this.doencas = doencas;
    }

    public static Candidatura fromDto(CandidaturaDTO cDto) {
        List<Doenca> doencas = cDto.doencas.stream().map(Doenca::fromDTO).collect(Collectors.toList());
        return new Candidatura(cDto.nome, cDto.sobrenome, cDto.morada, cDto.dataNascimento, cDto.localNascimento, cDto.cartaoCidadao,
                cDto.nacionalidade, cDto.profissao, cDto.grauEscolaridade, cDto.estadoCivil, cDto.veiculoProprio, doencas);
    }

    public CandidaturaDTO toDTO() {
        List<DoencaDTO> doencas = this.doencas.stream().map(Doenca::toDTO).collect(Collectors.toList());
        return new CandidaturaDTO(this.nome, this.sobrenome, this.morada, this.dataNascimento, this.localNascimento, this.cartaoCidadao,
                this.nacionalidade, null, this.profissao, this.grauEscolaridade, this.estadoCivil, this.veiculoProprio, doencas);
    }

}