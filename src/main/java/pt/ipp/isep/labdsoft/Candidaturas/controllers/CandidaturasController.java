package pt.ipp.isep.labdsoft.Candidaturas.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pt.ipp.isep.labdsoft.Candidaturas.dto.CandidaturaDTO;
import pt.ipp.isep.labdsoft.Candidaturas.dto.CredenciaisDTO;
import pt.ipp.isep.labdsoft.Candidaturas.services.CandidaturaService;

import java.util.List;


@RestController
@RequestMapping("candidaturas")
public final class CandidaturasController {

    @Autowired
    private CandidaturaService candidaturaService;


    @GetMapping("/")
    public ResponseEntity<List<CandidaturaDTO>> candidaturas() {
        List<CandidaturaDTO> candidaturas = candidaturaService.listAll();
        return new ResponseEntity<>(candidaturas, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<CandidaturaDTO> byId(@PathVariable Long id) {
        CandidaturaDTO candidatura = candidaturaService.byId(id);
        return new ResponseEntity<>(candidatura, HttpStatus.OK);
    }

    @PostMapping("/")
    public ResponseEntity<CredenciaisDTO> createCandidatura(@RequestBody CandidaturaDTO c) {
        System.out.println(c);
        CredenciaisDTO create = candidaturaService.createCandidatura(c);
        return new ResponseEntity<>(create, HttpStatus.ACCEPTED);

    }
}
