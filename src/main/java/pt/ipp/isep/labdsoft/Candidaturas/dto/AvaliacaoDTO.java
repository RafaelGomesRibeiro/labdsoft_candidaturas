package pt.ipp.isep.labdsoft.Candidaturas.dto;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString

public class AvaliacaoDTO {
    public Long id;
    public Long idMedico;
    public Long idPsicologo;
    public Long idUtente;
    public Boolean parecerPsicologico;
    public Boolean parecerMedico;
    public AvaliacaoMedicaDTO avaliacaoMedica;
    public AvaliacaoPsicologicaDTO avaliacaoPsicologica;
    public ConsultaPreEstudoDTO consultaPreEstudo;


}
