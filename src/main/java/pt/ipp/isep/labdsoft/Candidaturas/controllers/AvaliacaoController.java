package pt.ipp.isep.labdsoft.Candidaturas.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pt.ipp.isep.labdsoft.Candidaturas.domain.Protocolo;
import pt.ipp.isep.labdsoft.Candidaturas.domain.TipoGene;
import pt.ipp.isep.labdsoft.Candidaturas.domain.TipoSangue;
import pt.ipp.isep.labdsoft.Candidaturas.dto.AvaliacaoDTO;
import pt.ipp.isep.labdsoft.Candidaturas.dto.SubmissaoAvaliacaoMedicaDTO;
import pt.ipp.isep.labdsoft.Candidaturas.dto.SubmissaoAvaliacaoPsicologicaDTO;
import pt.ipp.isep.labdsoft.Candidaturas.services.AvaliacaoService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("avaliacoes")
public final class AvaliacaoController {

    @Autowired
    private AvaliacaoService avaliacaoService;


    @GetMapping("/")
    public ResponseEntity<List<AvaliacaoDTO>> avaliacoes() {
        List<AvaliacaoDTO> avaliacoes = avaliacaoService.listAll();
        return new ResponseEntity<>(avaliacoes, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<AvaliacaoDTO> byId(@PathVariable Long id) {
        AvaliacaoDTO avaliacao = avaliacaoService.byId(id);
        return new ResponseEntity<>(avaliacao, HttpStatus.OK);
    }

    @GetMapping("/utente/{id}")
    public ResponseEntity<AvaliacaoDTO> byUtenteId(@PathVariable Long id) {
        AvaliacaoDTO avaliacao = avaliacaoService.byUtente(id);
        return new ResponseEntity<>(avaliacao, HttpStatus.OK);
    }

    @GetMapping("/avaliacaoMedico/{idMedico}/porRealizar")
    public ResponseEntity<Iterable<AvaliacaoDTO>> byMedicoPorRealizar(@PathVariable Long idMedico) {
        Iterable<AvaliacaoDTO> avaliacoes = avaliacaoService.byMedicoPorRealizar(idMedico);
        return ResponseEntity.ok(avaliacoes);
    }

    @GetMapping("/avaliacaoMedico")
    public ResponseEntity<Long> medicoIdByUtente(@RequestParam(name = "utenteId") Long utenteId) {
        Long medicoId = avaliacaoService.medicoIdByUtente(utenteId);
        return ResponseEntity.ok(medicoId);
    }

    @GetMapping("/avaliacaoPsicologo/{idPsicologo}/porRealizar")
    public ResponseEntity<Iterable<AvaliacaoDTO>> byPsicologoPorRealizar(@PathVariable Long idPsicologo) {
        Iterable<AvaliacaoDTO> avaliacoes = avaliacaoService.byPsicologoPorRealizar(idPsicologo);
        return ResponseEntity.ok(avaliacoes);
    }


    @PutMapping("/avaliacaoPsicologica/{numPaciente}")
    public ResponseEntity<AvaliacaoDTO> updateAvaliacaoPsicologica(@RequestBody SubmissaoAvaliacaoPsicologicaDTO av, @PathVariable Long numPaciente) {
        AvaliacaoDTO avaliacao = avaliacaoService.updateAvaliacaoPsicologica(numPaciente, av);
        return new ResponseEntity<>(avaliacao, HttpStatus.NO_CONTENT);
    }

    @PutMapping("/avaliacaoMedica/{numPaciente}")
    public ResponseEntity<AvaliacaoDTO> updateAvaliacaoMedica(@RequestBody SubmissaoAvaliacaoMedicaDTO av, @PathVariable Long numPaciente) {
        AvaliacaoDTO avaliacao = avaliacaoService.updateAvaliacaoMedica(numPaciente, av);
        return new ResponseEntity<>(avaliacao, HttpStatus.NO_CONTENT);
    }

    @GetMapping("/protocolos/")
    public ResponseEntity<List<String>> protocolosExistentes(){
        return ResponseEntity.ok(Arrays.asList(Protocolo.values().toString()));
    }

    @GetMapping("/tiposGene")
    public ResponseEntity<List<String>> tiposGeneExistente(){
        return ResponseEntity.ok(Arrays.asList(TipoGene.values().toString()));
    }

    @GetMapping("/tiposSangue")
    public ResponseEntity<List<String>> tiposSangueExistente(){
        return ResponseEntity.ok(Arrays.asList(TipoSangue.values().toString()));
    }

    @GetMapping("/utentesPorProtocolo/")
    public ResponseEntity<Map<String, List<Long>>> utentesPorProtocolo(){
        return ResponseEntity.ok(avaliacaoService.getUtentesByProtocolo());
    }

    @GetMapping("/utentesPorTipoGene/")
    public ResponseEntity<Map<String, List<Long>>> utentesPorTipoGene(){
        return ResponseEntity.ok(avaliacaoService.getUtentesByTipoGene());
    }
}
