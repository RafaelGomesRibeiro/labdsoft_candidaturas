package pt.ipp.isep.labdsoft.Candidaturas.services.rabbitMQ.dispatcher;

import lombok.*;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public final class ParecerMedicoSubmetidoEvent implements Serializable {
    private String utenteId;
    private Boolean parecer;
}
