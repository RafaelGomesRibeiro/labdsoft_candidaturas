package pt.ipp.isep.labdsoft.Candidaturas.domain;

public enum TipoGene {
    AA,
    AG,
    GG,
    Outro
}
