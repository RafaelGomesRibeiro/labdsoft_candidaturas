package pt.ipp.isep.labdsoft.Candidaturas.dto;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString


public class AvaliacaoPsicologicaDTO {
    public String razaoParticipacao;
    public String partilhaDecisao;
    public String tempoParaDecidir;
}
