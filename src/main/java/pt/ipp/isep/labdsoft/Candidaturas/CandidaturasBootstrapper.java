/*
package pt.ipp.isep.labdsoft.Candidaturas;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import pt.ipp.isep.labdsoft.Candidaturas.controllers.PedidoMonitorizacaoController;
import pt.ipp.isep.labdsoft.Candidaturas.dto.PedidoMonitorizacaoDTO;

@Component
public class CandidaturasBootstrapper implements ApplicationRunner {

    private PedidoMonitorizacaoController pedidoMonitorizacaoController;

    public CandidaturasBootstrapper(PedidoMonitorizacaoController pedidoMonitorizacaoController) {
        this.pedidoMonitorizacaoController = pedidoMonitorizacaoController;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
//        registarPedidos(new String[]{"Verificar altura", "Verificar peso", "Verificar audição", "Verificar COVID19"}, new String[]{"01-01-2021 10:10", "02-01-2021 10:10", "04-01-2021 10:10", "03-01-2021 10:10"}
//                , new String[]{"descricao1", "descricao2", "descricao3", "descricao4"}, new Long[]{1L, 2L, 3L, 4L}, new Long[]{1L, 2L, 3L, 4L});
    }

    private PedidoMonitorizacaoDTO criarPedido(String nome, String descricao, String data, Long numPacient, Long medicoId) {
        return PedidoMonitorizacaoDTO.builder().data(data).descricao(descricao).nome(nome).numPaciente(numPacient).medicoId(medicoId).build();
    }

    private void registarPedidos(String[] nomes, String[] datas, String[] descricoes, Long[] idMedicos, Long[] numPacientes) {
        for (int i = 0; i < nomes.length; i++) {
            pedidoMonitorizacaoController.create(criarPedido(nomes[i], descricoes[i], datas[i], numPacientes[i], idMedicos[i]));
        }
    }
}
*/
