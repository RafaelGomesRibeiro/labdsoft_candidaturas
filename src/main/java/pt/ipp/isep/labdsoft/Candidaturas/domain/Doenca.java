package pt.ipp.isep.labdsoft.Candidaturas.domain;

import lombok.*;
import pt.ipp.isep.labdsoft.Candidaturas.dto.DoencaDTO;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
public final class Doenca implements Serializable {
    //@OneToOne
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String descricao;

    public Doenca(String descricao){
        this.descricao = descricao;
    }

    public static Doenca fromDTO(DoencaDTO dto){
        return new Doenca(dto.descricao);
    }

    public DoencaDTO toDTO(){
        return new DoencaDTO(this.descricao);
    }
}
