package pt.ipp.isep.labdsoft.Candidaturas.dto;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString


public class SubmissaoAvaliacaoPsicologicaDTO {
    public String razaoParticipacao;
    public String partilhaDecisao;
    public String tempoParaDecidir;
    public boolean parecer;


    public AvaliacaoPsicologicaDTO toAvaliacaoPsicologicaDTO() {
        return new AvaliacaoPsicologicaDTO(this.razaoParticipacao, this.partilhaDecisao, this.tempoParaDecidir);
    }
}
