package pt.ipp.isep.labdsoft.Candidaturas.utils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public final class DatesFormatter {

    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");

    private DatesFormatter() {
    }

    public static String convertToString(LocalDateTime time) {
        return time.format(FORMATTER);
    }

    public static LocalDateTime convertToLocalDateTime(String time) {
        return LocalDateTime.parse(time, FORMATTER);
    }
}
