package pt.ipp.isep.labdsoft.Candidaturas.domain;

public enum TipoSangue {
    A,
    B,
    AB,
    O
}
