package pt.ipp.isep.labdsoft.Candidaturas.services.rabbitMQ.listener;

import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;
import pt.ipp.isep.labdsoft.Candidaturas.services.AvaliacaoService;

@Component
public final class EventListener {

    private static final String GABINETES_EXCHANGE_KEY = "gabinetes";
    private static final String GABINETE_ALOCADO_EVENT_KEY = "A";
    private static final String EXCHANGE_TYPE = "direct";

    private static final String USERS_EXCHANGE_KEY = "utilizadores";
    private static final String EQUIPA_MEDICA_ALOCADA_EVENT_KEY = "A";

    private AvaliacaoService avaliacaoService;

    public EventListener(AvaliacaoService avaliacaoService) {
        this.avaliacaoService = avaliacaoService;
    }


    @RabbitListener(bindings = {@QueueBinding(value = @Queue(value = ""), exchange = @Exchange(value = GABINETES_EXCHANGE_KEY, type = EXCHANGE_TYPE), key = GABINETE_ALOCADO_EVENT_KEY)})
    public void listenGabineteAlocadoEvent(GabineteAlocadoEvent event) {
        try{
            avaliacaoService.setGabinete(Long.parseLong(event.getUtenteId()), event.getGabineteId());
        } catch (Exception e) {
            System.out.println("mensagem: " + e.getMessage());
            System.out.println("stack trace: " + e.getStackTrace().toString());
        }

    }

    @RabbitListener(bindings = {@QueueBinding(value = @Queue(value = ""), exchange = @Exchange(value = USERS_EXCHANGE_KEY, type = EXCHANGE_TYPE), key = EQUIPA_MEDICA_ALOCADA_EVENT_KEY)})
    public void listenEquipaMedicaAlocadaEvent(EquipaMedicaAlocadaEvent event) {
        try {
            avaliacaoService.createAvaliacao(Long.parseLong(event.getUtenteId()), Long.parseLong(event.getMedicoId()),
                    Long.parseLong(event.getPsicologoId()), event.getDataInicio(), event.getDataFim());
        } catch (Exception e) {
            System.out.println("mensagem: " + e.getMessage());
            System.out.println("stack trace: " + e.getStackTrace().toString());
        }

    }


}
