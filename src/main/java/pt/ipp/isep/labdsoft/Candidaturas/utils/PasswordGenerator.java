package pt.ipp.isep.labdsoft.Candidaturas.utils;

import java.util.UUID;

public final class PasswordGenerator {

    //not allow instantiation of the class
    private PasswordGenerator(){}

    public static String generatePassword(){
        return UUID.randomUUID().toString().substring(0,8);
    }
}
