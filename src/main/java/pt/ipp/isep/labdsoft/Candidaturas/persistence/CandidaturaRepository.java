package pt.ipp.isep.labdsoft.Candidaturas.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pt.ipp.isep.labdsoft.Candidaturas.domain.Candidatura;

@Repository
public interface CandidaturaRepository extends JpaRepository <Candidatura, Long> {
}
