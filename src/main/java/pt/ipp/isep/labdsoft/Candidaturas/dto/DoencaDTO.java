package pt.ipp.isep.labdsoft.Candidaturas.dto;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString

public class DoencaDTO {
    public String descricao;
}
