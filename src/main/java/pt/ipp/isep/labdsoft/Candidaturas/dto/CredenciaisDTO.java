package pt.ipp.isep.labdsoft.Candidaturas.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
public class CredenciaisDTO {
    public String username;
    public String password;
}
