package pt.ipp.isep.labdsoft.Candidaturas.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import pt.ipp.isep.labdsoft.Candidaturas.domain.Candidatura;
import pt.ipp.isep.labdsoft.Candidaturas.dto.CandidaturaDTO;
import pt.ipp.isep.labdsoft.Candidaturas.dto.CredenciaisDTO;
import pt.ipp.isep.labdsoft.Candidaturas.persistence.CandidaturaRepository;
import pt.ipp.isep.labdsoft.Candidaturas.services.rabbitMQ.dispatcher.CandidaturaSubmetidaEvent;
import pt.ipp.isep.labdsoft.Candidaturas.services.rabbitMQ.dispatcher.EventDispatcher;
import pt.ipp.isep.labdsoft.Candidaturas.utils.DatesFormatter;
import pt.ipp.isep.labdsoft.Candidaturas.utils.PasswordGenerator;

import java.time.LocalDateTime;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

@Service
public class CandidaturaServiceImpl implements CandidaturaService {

    @Autowired
    private CandidaturaRepository repo;

    @Autowired
    private EventDispatcher eventDispatcher;

    @Autowired
    private AES256Encrypter encrypter;

    private final WebClient webClient = WebClient.create();

    @Value("${USERS_BASE_URL}")
    private String usersBaseUrl;

    @Override
    public List<CandidaturaDTO> listAll() {
        return repo.findAll().stream().map(c -> c.toDTO()).collect(Collectors.toList());
    }

    @Override
    public CredenciaisDTO createCandidatura(CandidaturaDTO dto) {
        if (!dto.veiculoProprio) {
            throw new IllegalArgumentException("Não se pode candidatar se não possuir veículo próprio");
        }
//        boolean duplicatedEmail = webClient.get().uri(usersBaseUrl + String.format("exists/%s", dto.email)).retrieve().bodyToMono(Boolean.class).block();
//        if (duplicatedEmail)
//            throw new IllegalArgumentException("Email já se encontra em uso");
        boolean duplicatedCartaoCidadao = repo.findAll().stream().anyMatch(c -> encrypter.decryptString(c.cartaoCidadao(), c.salt()).equalsIgnoreCase(dto.cartaoCidadao));
        if (duplicatedCartaoCidadao)
            throw new IllegalArgumentException("Cartão de cidadão já se encontra registado");

        Candidatura c = Candidatura.fromDto(dto);
        String candidaturaSalt = encrypter.generateSalt();
        c.salt(candidaturaSalt);
        c.nome(encrypter.encryptString(c.nome(), candidaturaSalt));
        c.sobrenome(encrypter.encryptString(c.sobrenome(), candidaturaSalt));
        c.cartaoCidadao(encrypter.encryptString(c.cartaoCidadao(), candidaturaSalt));
        c.morada(encrypter.encryptString(c.morada(), candidaturaSalt));
        c.localNascimento(encrypter.encryptString(c.localNascimento(), candidaturaSalt));
        c = repo.save(c);

        String generatedPassword = PasswordGenerator.generatePassword();

        CandidaturaSubmetidaEvent cE = CandidaturaSubmetidaEvent.builder().nome(dto.nome).sobrenome(dto.sobrenome).
                candidaturaId(String.valueOf(c.id())).utenteId(String.valueOf(c.id())).utentePassword(generatedPassword)
                .email(dto.email).requestedAt(DatesFormatter.convertToString(LocalDateTime.now())).build();

        eventDispatcher.dispatchCandidaturaSubmetidaEvent(cE);
        return new CredenciaisDTO(String.valueOf(c.id()), generatedPassword);
    }

    @Override
    public CandidaturaDTO byId(Long id) {
        Candidatura candidatura = repo.findById(id).orElseThrow(() -> new NoSuchElementException("Candidatura com id " + id + " nao existe"));
        return candidatura.toDTO();
    }

}
