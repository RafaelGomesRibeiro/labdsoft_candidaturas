package pt.ipp.isep.labdsoft.Candidaturas.domain;

import lombok.*;
import pt.ipp.isep.labdsoft.Candidaturas.dto.AvaliacaoPsicologicaDTO;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
public final class AvaliacaoPsicologica implements Serializable {
    //@OneToOne
    //private Candidatura candidatura;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String razaoParticipacao;
    private String partilhaDecisao;
    private String tempoParaDecidir;

    public AvaliacaoPsicologica(String razaoParticipacao, String partilhaDecisao, String tempoParaDecidir){
        this.razaoParticipacao = razaoParticipacao;
        this.partilhaDecisao = partilhaDecisao;
        this.tempoParaDecidir = tempoParaDecidir;
    }

    public static AvaliacaoPsicologica fromDTO(AvaliacaoPsicologicaDTO dto){
        return new AvaliacaoPsicologica(dto.razaoParticipacao, dto.partilhaDecisao, dto.tempoParaDecidir);
    }

    public AvaliacaoPsicologicaDTO toDTO(){
        return new AvaliacaoPsicologicaDTO(this.razaoParticipacao, this.partilhaDecisao, this.tempoParaDecidir);
    }
}