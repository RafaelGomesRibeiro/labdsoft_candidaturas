package pt.ipp.isep.labdsoft.Candidaturas.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pt.ipp.isep.labdsoft.Candidaturas.domain.AvaliacaoPsicologica;

@Repository
public interface AvaliacaoPsicologicaRepository extends JpaRepository <AvaliacaoPsicologica, Long> {
}
