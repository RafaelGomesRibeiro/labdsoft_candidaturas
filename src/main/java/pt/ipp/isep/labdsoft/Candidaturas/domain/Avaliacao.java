package pt.ipp.isep.labdsoft.Candidaturas.domain;

import lombok.*;
import pt.ipp.isep.labdsoft.Candidaturas.dto.AvaliacaoDTO;
import pt.ipp.isep.labdsoft.Candidaturas.dto.AvaliacaoMedicaDTO;
import pt.ipp.isep.labdsoft.Candidaturas.dto.AvaliacaoPsicologicaDTO;
import pt.ipp.isep.labdsoft.Candidaturas.dto.ConsultaPreEstudoDTO;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
public class Avaliacao implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private Long idMedico;
    private Long idPsicologo;
    private Long idUtente;
    private Boolean parecerPsicologico;
    private Boolean parecerMedico;
    @OneToOne(cascade = CascadeType.ALL)
    private AvaliacaoMedica avaliacaoMedica;
    @OneToOne(cascade = CascadeType.ALL)
    private AvaliacaoPsicologica avaliacaoPsicologica;
    @OneToOne(cascade = CascadeType.ALL)
    private ConsultaPreEstudo consulta;

    public Avaliacao(Long idMedico, Long idPsicologo, Long idUtente, Boolean parecerPsicologico, Boolean parecerMedico, AvaliacaoMedica avaliacaoMedica, AvaliacaoPsicologica avaliacaoPsicologica, ConsultaPreEstudo consulta){
        this.idMedico = idMedico;
        this.idPsicologo = idPsicologo;
        this.idUtente = idUtente;
        this.parecerPsicologico = parecerPsicologico;
        this.parecerMedico = parecerMedico;
        this.avaliacaoMedica = avaliacaoMedica;
        this.avaliacaoPsicologica = avaliacaoPsicologica;
        this.consulta = consulta;
    }

    public static Avaliacao fromDTO(AvaliacaoDTO dto){
        AvaliacaoPsicologica avaliacaoPsicologica = AvaliacaoPsicologica.fromDTO(dto.avaliacaoPsicologica);
        AvaliacaoMedica avaliacaoMedica = AvaliacaoMedica.fromDTO(dto.avaliacaoMedica);
        ConsultaPreEstudo consulta = ConsultaPreEstudo.fromDTO(dto.consultaPreEstudo);
        return new Avaliacao(dto.idMedico, dto.idPsicologo, dto.idUtente, dto.parecerPsicologico, dto.parecerMedico, avaliacaoMedica, avaliacaoPsicologica, consulta);
    }

    public AvaliacaoDTO toDTO(){
        AvaliacaoMedicaDTO avaliacaoMedicaDTO = this.avaliacaoMedica.toDTO();
        AvaliacaoPsicologicaDTO avaliacaoPsicologicaDTO = this.avaliacaoPsicologica.toDTO();
        ConsultaPreEstudoDTO consultaPreEstudoDTO = this.consulta.toDTO();
        return new AvaliacaoDTO(this.id, this.idMedico, this.idPsicologo, this.idUtente, this.parecerPsicologico, this.parecerMedico, avaliacaoMedicaDTO, avaliacaoPsicologicaDTO, consultaPreEstudoDTO);
    }
}

