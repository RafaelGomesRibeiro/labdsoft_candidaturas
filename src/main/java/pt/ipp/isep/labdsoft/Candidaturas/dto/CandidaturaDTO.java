package pt.ipp.isep.labdsoft.Candidaturas.dto;

import lombok.*;

import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
public class CandidaturaDTO {
    public String nome;
    public String sobrenome;
    public String morada;
    public Date dataNascimento;
    public String localNascimento;
    public String cartaoCidadao;
    public String nacionalidade;
    public String email;
    public String profissao;
    public String grauEscolaridade;
    public String estadoCivil;
    public boolean veiculoProprio;
    public List<DoencaDTO> doencas;


}
