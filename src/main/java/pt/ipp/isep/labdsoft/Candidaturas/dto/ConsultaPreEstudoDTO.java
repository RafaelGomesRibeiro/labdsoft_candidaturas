package pt.ipp.isep.labdsoft.Candidaturas.dto;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ConsultaPreEstudoDTO {
    public String inicio;
    public String fim;
    public String idGabinete;
}
