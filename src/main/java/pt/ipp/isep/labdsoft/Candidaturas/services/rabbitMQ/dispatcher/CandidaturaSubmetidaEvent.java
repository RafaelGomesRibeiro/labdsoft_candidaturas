package pt.ipp.isep.labdsoft.Candidaturas.services.rabbitMQ.dispatcher;

import lombok.*;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public final class CandidaturaSubmetidaEvent implements Serializable {
    private String requestedAt;
    private String candidaturaId;
    private String utenteId;
    private String utentePassword;
    private String nome;
    private String sobrenome;
    private String email;
}
