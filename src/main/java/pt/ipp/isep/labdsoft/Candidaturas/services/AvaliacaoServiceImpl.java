package pt.ipp.isep.labdsoft.Candidaturas.services;

import org.springframework.stereotype.Service;
import pt.ipp.isep.labdsoft.Candidaturas.domain.*;
import pt.ipp.isep.labdsoft.Candidaturas.dto.AvaliacaoDTO;
import pt.ipp.isep.labdsoft.Candidaturas.dto.SubmissaoAvaliacaoMedicaDTO;
import pt.ipp.isep.labdsoft.Candidaturas.dto.SubmissaoAvaliacaoPsicologicaDTO;
import pt.ipp.isep.labdsoft.Candidaturas.persistence.AvaliacaoRepository;
import pt.ipp.isep.labdsoft.Candidaturas.services.rabbitMQ.dispatcher.EventDispatcher;
import pt.ipp.isep.labdsoft.Candidaturas.services.rabbitMQ.dispatcher.ParecerMedicoSubmetidoEvent;
import pt.ipp.isep.labdsoft.Candidaturas.utils.DatesFormatter;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class AvaliacaoServiceImpl implements AvaliacaoService {


    private AvaliacaoRepository repository;
    private EventDispatcher eventDispatcher;

    public AvaliacaoServiceImpl(AvaliacaoRepository repository, EventDispatcher eventDispatcher) {
        this.repository = repository;
        this.eventDispatcher = eventDispatcher;
    }

    @Override
    public List<AvaliacaoDTO> listAll() {
        return repository.findAll().stream().map(Avaliacao::toDTO).collect(Collectors.toList());
    }

    @Override
    public AvaliacaoDTO createAvaliacao(Long idUtente, Long idMedico, Long idPsicologo, String inicio, String fim) {
        ConsultaPreEstudo cpe = ConsultaPreEstudo.builder().inicio(DatesFormatter.convertToLocalDateTime(inicio))
                .fim(DatesFormatter.convertToLocalDateTime(fim))
                .build();
        Avaliacao av = Avaliacao.builder().idMedico(idMedico).idPsicologo(idPsicologo)
                .idUtente(idUtente).consulta(cpe).build();
        AvaliacaoPsicologica avaliacaoPsicologica = AvaliacaoPsicologica.builder().build();
        AvaliacaoMedica avaliacaoMedica = AvaliacaoMedica.builder().build();
        av.setAvaliacaoMedica(avaliacaoMedica);
        av.setAvaliacaoPsicologica(avaliacaoPsicologica);

        return repository.save(av).toDTO();
        //TODO verificar se é necessario lançar mais algum evento neste fluxo
    }


    @Override
    public AvaliacaoDTO byId(Long id) {
        Avaliacao avaliacao = repository.findById(id).orElseThrow(() -> new NoSuchElementException("Avaliação com id " + id + " nao existe"));
        // System.out.println(avaliacao.getConsulta());
        return avaliacao.toDTO();
    }

    @Override
    public AvaliacaoDTO byUtente(Long idUtente) {
        Avaliacao avaliacao = repository.findByIdUtente(idUtente).orElseThrow(() -> new NoSuchElementException("Avaliação com do utente " + idUtente + " nao existe"));
        return avaliacao.toDTO();
    }

    @Override
    public AvaliacaoDTO updateAvaliacaoPsicologica(Long id, SubmissaoAvaliacaoPsicologicaDTO dto) {
        Avaliacao avaliacao = repository.findByIdUtente(id).orElseThrow(() -> new NoSuchElementException("Utente com id " + id + " não tem avaliação associada"));
        AvaliacaoPsicologica avaliacaoPsicologica = AvaliacaoPsicologica.fromDTO(dto.toAvaliacaoPsicologicaDTO());
        avaliacao.setAvaliacaoPsicologica(avaliacaoPsicologica);
        avaliacao.setParecerPsicologico(dto.parecer);
        Avaliacao updated = repository.save(avaliacao);
        return updated.toDTO();

    }

    @Override
    public AvaliacaoDTO updateAvaliacaoMedica(Long id, SubmissaoAvaliacaoMedicaDTO dto) {
        Avaliacao avaliacao = repository.findByIdUtente(id).orElseThrow(() -> new NoSuchElementException("Utente com id " + id + " não tem avaliação associada"));
        AvaliacaoMedica avaliacaoMedica = AvaliacaoMedica.fromDTO(dto.toAvaliacaoMedicaDTO());
        avaliacao.setAvaliacaoMedica(avaliacaoMedica);
        avaliacao.setParecerMedico(dto.parecer);
        Avaliacao updated = repository.save(avaliacao);
        //Lancar evento de parecer medico submetido
        ParecerMedicoSubmetidoEvent e = ParecerMedicoSubmetidoEvent.builder().utenteId(String.valueOf(avaliacao.getIdUtente())).parecer(updated.getParecerMedico()).build();
        eventDispatcher.dispatchParecerMedicoSubmetidoEvent(e);
        return updated.toDTO();
    }

    @Override
    public AvaliacaoDTO setGabinete(Long idUtente, String idGabinete) {
        Avaliacao avaliacao = repository.findByIdUtente(idUtente).orElseThrow(() -> new NoSuchElementException("Avaliação do Utente " + idUtente + " nao existe"));
        ;
        avaliacao.getConsulta().setIdGabinete(idGabinete);
        repository.save(avaliacao);
        return avaliacao.toDTO();
    }

    @Override
    public Iterable<AvaliacaoDTO> byMedicoPorRealizar(Long idMedico) {
        return repository.findAvaliacaosByIdMedico(idMedico).stream().filter(av -> av.getParecerMedico() == null && av.getConsulta().getIdGabinete() != null)
                .map(Avaliacao::toDTO).collect(Collectors.toList());
    }


    @Override
    public Iterable<AvaliacaoDTO> byPsicologoPorRealizar(Long idPsicologo) {
        return repository.findAvaliacaosByIdPsicologo(idPsicologo).stream().filter(av -> av.getParecerPsicologico() == null && av.getConsulta().getIdGabinete() != null)
                .map(Avaliacao::toDTO).collect(Collectors.toList());
    }

    @Override
    public Long medicoIdByUtente(Long idUtente) {
        Avaliacao avaliacao = repository.findByIdUtente(idUtente).orElseThrow(() -> new NoSuchElementException("Utente com id " + idUtente + " não tem avaliação associada"));
        return avaliacao.getIdMedico();
    }

    @Override
    public Map<String, List<Long>> getUtentesByProtocolo() {
        List<AvaliacaoDTO> avaliacoes = listAll();
        Map<String, List<Long>> utentesPorProtocolo = new HashMap<String, List<Long>>();
        for (AvaliacaoDTO av : avaliacoes) {
            if (utentesPorProtocolo.containsKey(av.avaliacaoMedica.protocolo)) {
                List<Long> utentesAtuais = utentesPorProtocolo.get(av.avaliacaoMedica.protocolo);
                utentesAtuais.add(av.idUtente);
                utentesPorProtocolo.put(av.avaliacaoMedica.protocolo, utentesAtuais);
            } else {
                List<Long> utentes = new ArrayList<>();
                utentes.add(av.idUtente);
                utentesPorProtocolo.put(av.avaliacaoMedica.protocolo, utentes);
            }
        }
        return utentesPorProtocolo;
    }

    @Override
    public Map<String, List<Long>> getUtentesByTipoGene() {
        List<AvaliacaoDTO> avaliacoes = listAll();
        Map<String, List<Long>> utentesPorTipoGene = new LinkedHashMap<String, List<Long>>();
        for (TipoGene gene : TipoGene.values()) {
            utentesPorTipoGene.put(gene.toString(), new ArrayList<>());
        }
        for (AvaliacaoDTO av : avaliacoes) {
            if (av.avaliacaoMedica.tipoGene == null) continue;
            List<Long> utentesAtuais = utentesPorTipoGene.get(av.avaliacaoMedica.tipoGene);
            utentesAtuais.add(av.idUtente);
            utentesPorTipoGene.put(av.avaliacaoMedica.tipoGene, utentesAtuais);

        }
        return utentesPorTipoGene;
    }


}
