package pt.ipp.isep.labdsoft.Candidaturas.domain;

import lombok.*;
import pt.ipp.isep.labdsoft.Candidaturas.dto.ConsultaPreEstudoDTO;
import pt.ipp.isep.labdsoft.Candidaturas.utils.DatesFormatter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
@Builder
public class ConsultaPreEstudo implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private LocalDateTime inicio;
    private LocalDateTime fim;
    private String idGabinete;

    public ConsultaPreEstudo(LocalDateTime inicio, LocalDateTime fim, String idGabinete) {
        this.inicio = inicio;
        this.fim = fim;
        this.idGabinete = idGabinete;
    }

    public static ConsultaPreEstudo fromDTO(ConsultaPreEstudoDTO dto) {
        return new ConsultaPreEstudo(DatesFormatter.convertToLocalDateTime(dto.inicio), DatesFormatter.convertToLocalDateTime(dto.fim), dto.idGabinete);
    }

    public ConsultaPreEstudoDTO toDTO() {
        return new ConsultaPreEstudoDTO(DatesFormatter.convertToString(this.inicio), DatesFormatter.convertToString(this.fim), this.idGabinete);
    }
}
