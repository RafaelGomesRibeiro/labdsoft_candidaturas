#!/bin/sh

./gradlew build -x check
docker container inspect rabbitMQ > /dev/null || docker-compose up -d rabbit
docker container inspect databases > /dev/null || docker-compose up -d postgres
docker network ls|grep labdsoft_network > /dev/null || docker network create --driver bridge labdsoft_network
container_name=`docker ps -q -f name=rabbitMQ`

if [ -z "$container_name" ]
then
    echo $container_name
    echo 'Rabbit not running, starting it'
    #docker-compose up -d --build rabbit
    docker start rabbitMQ
else
    echo 'Rabbit already running'
fi

container_name2=`docker ps -q -f name=databases`

if [ -z "$container_name2" ]
then
    echo $container_name2
    echo 'Databases not running, starting it'
    #docker-compose up -d --build rabbit
    docker start databases
else
    echo 'Databases already running'
fi

docker-compose up -d --build web && sleep 10 && xdg-open http://localhost:9104/api/documentation